#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
迭代字典
"""
legends = {('Poe', 'author'): (1809, 1849, 1976), ('Guadi', 'architect'): (1852, 1906, 1987)}

for eachLegend in legends:
    print('Name: %s\tOccupation: %s' % eachLegend)
    print(' Birth: %s\tDeath:%s\tAlbum: %s\n' % legends[eachLegend])


# Name: Guadi	Occupation: architect
#  Birth: 1852	Death:1906	Album: 1987
#
# Name: Poe	Occupation: author
#  Birth: 1809	Death:1849	Album: 1976
