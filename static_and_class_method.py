#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
Python program to demonstrate use of class method and static method.
"""
from datetime import date


class Person(object):
    """
    A Person Class

    instance data property: 1. name
                            2. age

    """

    def __init__(self, name, age):
        """

        :param name:
        :param age:
        """
        self.name = name
        self.age = age

    @classmethod
    def from_birth_year(cls, name, year):
        """
        a class method to create a Person object by birth year.

        :param name:
        :param year:
        :return:

        """
        return cls(name, date.today().year - year)

    @staticmethod
    def is_adult(age):
        """
        a static method to check if Person object is adult or not.
        :param age:
        :return: True or False
        """
        return age >= 18


james = Person('James', 17)
chris = Person.from_birth_year('Chris', 1994)

print(Person.is_adult(james.age))
print(Person.is_adult(chris.age))
