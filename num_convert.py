#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
练习function references
"""


def convert(func, seq):
    """
    转换函数
    :param func:
    :param seq:
    :return:
    """

    return [func(each_num) for each_num in seq]


my_seq = [123, 45.67, -6.2e8, 99999999]
print(convert(int, my_seq))
print(convert(float, my_seq))
