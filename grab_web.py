#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
从Web页面上获得第一和最后的非空格行
"""

from urllib.request import urlretrieve


def first_non_blank(lines):
    """

    :param lines:
    :return:
    """

    for each_line in lines:
        if not each_line.strip():
            continue
        else:
            return each_line


def first_last(web_page):
    """

    :param web_page:
    :return:
    """

    with open(web_page) as f:
        lines = f.readlines()
        print(first_non_blank(lines))
        lines.reverse()
        print(first_non_blank(lines))


def call_back(block_num, block_size, total_size):
    """
    urlretrieve函数的reporthook
    :param block_num: 已经下载的块数
    :param block_size: 数据块大小
    :param total_size: 远程文件大小
    :return:
    """

    percent = block_num * block_size / total_size
    if percent > 1.0:
        percent = 1.0
    print('{:.2%}'.format(percent))  # 0.25	{:.2%}	25.00%	百分比格式


def download(url='https://www.baidu.com', process=first_last):
    try:
        ret_val = urlretrieve(url, 'a.html', call_back)  # 从网络上下载url表示的对象到本地，并显示进度
        print(ret_val)
        ret_val = ret_val[0]
    except IOError:
        ret_val = None
    if ret_val:
        process(ret_val)


if __name__ == '__main__':
    download()
