#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
装饰器
"""

from time import ctime, sleep


def ts_func(func):
    """
    装饰器
    :param func:
    :return: wrapped_func
    """

    def wrapped_func():
        print('[{}] {}() called'.format(ctime(), func.__name__))
        return func()

    return wrapped_func


# foo = ts_func(foo)
@ts_func
def foo():
    pass


foo()
print(foo.__name__)
sleep(4)

for i in range(2):
    sleep(1)
    foo()
