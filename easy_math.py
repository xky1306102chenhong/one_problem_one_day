# -*- coding: utf-8 -*-
"""
[0,99]整数的加减
"""

from operator import add, sub
from random import randint, choice

ops = {'+': add, '-': sub}
MAX_TRIES = 2


def do_prob():
    """
    出题及答题
    :return:
    """
    op = choice('+-')  # 随机取运算符
    nums = [randint(0, 100) for i in range(2)]  # 随机取[0,99]的整数
    nums.sort(reverse=True)
    ans = ops[op](*nums)  # 计算答案
    problem = '{} {} {} = '.format(nums[0], op, nums[1])  # 题目
    counter = 0  # 答题次数
    while True:
        try:
            if int(input(problem)) == ans:
                print('correct')
                break
            if MAX_TRIES == counter:
                print('answer is\n{}{}'.format(problem, ans))
            else:
                print('incorrect... try again')
                counter += 1
        except (KeyboardInterrupt, ValueError, EOFError):
            print('invalid input... try again')


def main():
    while True:
        do_prob()
        try:
            option = input('Again? [y]').lower()
            if option and option[0] == 'n':
                break
        except (KeyboardInterrupt, EOFError):
            break


if __name__ == '__main__':
    main()
