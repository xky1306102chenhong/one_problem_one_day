#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
计算函数的执行时间
"""
from time import clock


def timeit(func, *args, **kwargs):
    """
    计算函数的执行时间
    :param func:
    :param args:
    :param kwargs:
    :return:
    """

    start_time = clock()
    try:
        ret_val = func(*args, **kwargs)
        end_time = clock()
        total_time = end_time - start_time
        result = (True, total_time, ret_val)
    except Exception as e:
        end_time = clock()
        total_time = end_time - start_time
        result = (False, total_time, e.args[0])

    return result


def test():
    """

    :return:
    """

    funcs = (int, float)
    vals = (1234, 12.34, '1234', '12.34')

    for each_func in funcs:
        print('_' * 20)
        for each_val in vals:
            ret_val = timeit(each_func, each_val)
            if ret_val[0]:
                print('{}({}) = {} {}'.format(each_func.__name__, each_val, ret_val[2], ret_val[1]))
            else:
                print('{}({}) = FAILED: {} {}'.format(each_func.__name__, each_val, ret_val[2], ret_val[1]))


if __name__ == '__main__':
    test()