#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
计算a.txt文件非空白字符的个数
"""


import os

# 查看文件大小（bytes）
a_size = os.stat('a.txt').st_size
print(a_size)

# 列表解析（小文件）
# with open('a.txt') as f:
#     print(sum([len(word) for line in f for word in line.split()]))

# 生成器表达式 = 生成器 + 列表解析
with open('a.txt') as f:
    print(sum((len(word) for line in f for word in line.split())))