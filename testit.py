#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
测试函数是否被正确的执行
"""


def testit(func, *args, **kwargs):
    """
    测试函数是否被正确的执行
    :param func:
    :param args:
    :param kwargs:
    :return:
    """

    try:
        ret_val = func(*args, **kwargs)
        result = (True, ret_val)
    except Exception as e:
        result = (False, e.args[0])

    return result


def test():
    """

    :return:
    """

    funcs = (int, float)
    vals = (1234, 12.34, '1234', '12.34')

    for each_func in funcs:
        print('_' * 20)
        for each_val in vals:
            ret_val = testit(each_func, each_val)
            if ret_val[0]:
                print('{}({}) = {}'.format(each_func.__name__, each_val, ret_val[1]))
            else:
                print('{}({}) = FAILED: {}'.format(each_func.__name__, each_val, ret_val[1]))


if __name__ == '__main__':
    test()
