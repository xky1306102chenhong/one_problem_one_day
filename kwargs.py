#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
关键字变量参数（字典）
"""


def dict_var_args(arg1, arg2='default', **arg3):
    """
    测试**kwargs的用法
    :param arg1:
    :param arg2:
    :param arg3:
    :return:
    """

    print('arg1: {}'.format(arg1))
    print('arg2: {}'.format(arg2))
    for each_arg_key in arg3.keys():
        print('args3: {}'.format(arg3[each_arg_key]))


if __name__ == '__main__':
    dict_var_args('123', a=1, b=2, c=3)


# output:
# arg1: 123
# arg2: default
# args3: 2
# args3: 1
# args3: 3
